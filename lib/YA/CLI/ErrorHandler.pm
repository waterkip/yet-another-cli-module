package YA::CLI::ErrorHandler;
our $VERSION = '0.007';
use Moo;
use namespace::autoclean;

# ABSTRACT: The default action handler

with 'YA::CLI::ActionRole';

sub action { 'default' }

sub run {
    my $self = shift;
    my $h = $self->as_help(1, $self->message);
    return $h->run;
}

__PACKAGE__->meta->make_immutable;

__END__

=for Pod::Coverage action run message

=head1 DESCRIPTION

This fallback action handler for if there isn't an action handler defined or
called.

=head1 METHODS

It implements all the methods defined in L<YA::CLI::ActionRole>.

